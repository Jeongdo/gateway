################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
source/HL_emac.obj: ../source/HL_emac.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_emac.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_epc.obj: ../source/HL_epc.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_epc.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_errata.obj: ../source/HL_errata.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_errata.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_esm.obj: ../source/HL_esm.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_esm.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_mdio.obj: ../source/HL_mdio.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_mdio.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_nmpu.obj: ../source/HL_nmpu.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_nmpu.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_notification.obj: ../source/HL_notification.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_notification.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_phy_dp83640.obj: ../source/HL_phy_dp83640.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_phy_dp83640.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_phy_tlk111.obj: ../source/HL_phy_tlk111.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_phy_tlk111.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_pinmux.obj: ../source/HL_pinmux.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_pinmux.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sci.obj: ../source/HL_sci.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sci.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_core.obj: ../source/HL_sys_core.asm $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_core.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_dma.obj: ../source/HL_sys_dma.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_dma.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_intvecs.obj: ../source/HL_sys_intvecs.asm $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_intvecs.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_main.obj: ../source/HL_sys_main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_main.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_mpu.obj: ../source/HL_sys_mpu.asm $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_mpu.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_pcr.obj: ../source/HL_sys_pcr.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_pcr.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_phantom.obj: ../source/HL_sys_phantom.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_phantom.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_pmm.obj: ../source/HL_sys_pmm.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_pmm.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_pmu.obj: ../source/HL_sys_pmu.asm $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_pmu.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_startup.obj: ../source/HL_sys_startup.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_startup.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_sys_vim.obj: ../source/HL_sys_vim.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_sys_vim.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/HL_system.obj: ../source/HL_system.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/HL_system.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

source/lwip_functions.obj: ../source/lwip_functions.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/bin/armcl" -mv7R5 --code_state=32 --float_support=VFPv3D16 --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/ports/hdk/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include" --include_path="C:/ti/Hercules/HALCoGen EMAC Driver with lwIP Demonstration/v00.03.00/lwip-1.4.1/src/include/ipv4" --include_path="C:/Users/USER/workspace_v7/TMS570LC4357_lwIP/TMS570LC4357_lwIP/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.6.LTS/include" --define=_TMS570LC43x_ -g --diag_warning=225 --diag_wrap=off --display_error_number --enum_type=packed --abi=eabi --preproc_with_compile --preproc_dependency="source/lwip_functions.d_raw" --obj_directory="source" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


