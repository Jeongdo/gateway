

/* Include Files */

#include "HL_sys_common.h"


#include "HL_gio.h"
#include "HL_emac.h"
#include "lwip/opt.h"
#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/tcp.h"

#include "echo.h"

static struct tcp_pcb *echo_pcb;
err_t err;

ip_addr_t ipaddr;

enum echo_states
{
  ES_NONE = 0,
  ES_ACCEPTED,
  ES_RECEIVED,
  ES_CLOSING
};

struct echo_state
{
  u8_t state;
  u8_t retries;
  struct tcp_pcb *pcb;
  /* pbuf (chain) to recycle */
  struct pbuf *p;
};


extern void EMAC_LwIP_Main (uint8_t * emacAddress);
extern err_t echo_accept(void *arg, struct tcp_pcb *newpcb, err_t err);
extern err_t echo_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err);
extern void echo_error(void *arg, err_t err);
extern err_t echo_poll(void *arg, struct tcp_pcb *tpcb);
extern err_t echo_sent(void *arg, struct tcp_pcb *tpcb, u16_t len);
void Socket_connect();
void tx_data();
void Latest_Date_Receive();
void Database_access();
void File_error_process();

uint8   emacAddress[6U] =   {0x00U, 0x08U, 0xEEU, 0x03U, 0xA6U, 0x6CU};
uint32  emacPhyAddress  =   1U;


void main(void)
{

    EMAC_LwIP_Main(emacAddress);
    while(1)
    {
        Socket_connect();
        Latest_Date_Receive();
        Database_access();
        tx_data();
    }

/* USER CODE END */
}

void Socket_connect()
{
    echo_pcb = tcp_new(); //create pcb

    IP4_ADDR(&ipaddr,192,168,1,11); //server Ip

    err = tcp_connect(echo_pcb, &ipaddr, 6000, echo_accept); //connect to the server

    tcp_arg(echo_pcb, NULL);
}

void tx_data()
{
          tcp_sent(echo_pcb, echo_sent);

          struct pbuf *p = pbuf_alloc(PBUF_TRANSPORT, 1024, PBUF_RAM);
          unsigned char buffer_send[1024] = "DATA";
          p->payload = buffer_send;
          p->len = 1024;
          p->tot_len = 1024;

          err = tcp_write(echo_pcb, p->payload, p->len, 1);
          err = tcp_output(echo_pcb);

          tcp_close(echo_pcb);

     if (err != ERR_OK)
     {
        Socket_connect();
     }

}

err_t echo_accept(void *arg, struct tcp_pcb *newpcb, err_t err)
{
  err_t ret_err;
  struct echo_state *es;

  LWIP_UNUSED_ARG(arg);
  LWIP_UNUSED_ARG(err);

  /* Unless this pcb should have NORMAL priority, set its priority now.
     When running out of pcbs, low priority pcbs can be aborted to create
     new pcbs of higher priority. */
  tcp_setprio(newpcb, TCP_PRIO_MIN);

  es = (struct echo_state *)mem_malloc(sizeof(struct echo_state));

  if (es != NULL)
  {
    es->state = ES_ACCEPTED;
    es->pcb = newpcb;
    es->retries = 0;
    es->p = NULL;
    /* pass newly allocated es to our callbacks */
    tcp_arg(newpcb, es);
    tcp_err(newpcb, echo_error);
    tcp_poll(newpcb, echo_poll, 0);
    ret_err = ERR_OK;
  }
  else
  {
    ret_err = ERR_MEM;
  }
  return ret_err;
}


void Latest_Date_Receive()
{
    //TBD...
    return;
}

void Database_access(void)
{
    File_error_process();
    //TBD...
    return;
}

void File_error_process(void)
{
    //TBD...
    return;
}
